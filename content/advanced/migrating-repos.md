---
eleventyNavigation:
  key: MigratingRepos
  title: Migrating Repositories
  parent: AdvancedUsage
---

Using a distributed version control software system (like Git) allows you to keep control of the data inside the repository.
If you want to download a local copy of your files, you can `git clone` your repo, or download the repo from the web interface.

This works well for moving files, but when you want to migrate metadata like issues, releases and a wiki, you can use the migration tool.

<picture>
  <source srcset="/assets/images/advanced/migrating-repos/new-migration.webp" type="image/webp">
  <img src="/assets/images/advanced/migrating-repos/new-migration.png">
</picture>

On Codeberg, you can click on the plus symbol on the top right, and then select [`New Migration`](https://codeberg.org/repo/migrate) on the dropdown to access the migration tool. 

## Selecting your host
Once you're at the [`New Migration`](https://codeberg.org/repo/migrate) page, you can select the Git host you are migrating from. If it's not on the list, you will have to select the Git option. This will not migrate metadata.

Here we document instructions for migrating specific to each service. Once you've followed through, move on to [`Starting Migration`](#starting-migration).

### Migrating from Git
<picture>
  <source srcset="/assets/images/advanced/migrating-repos/git-migration.webp" type="image/webp">
  <img src="/assets/images/advanced/migrating-repos/git-migration.png">
</picture>

Here's an explanation of some fields on the [Git migration page](https://codeberg.org/repo/migrate?service_type=1):
- **Migrate / Clone From URL**: This is the URL to your repository. For example: `https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git`.
- **Username and Password**: Optionally, if authentication is required to access the repo, you can enter a username and password here.

### Migrating from Gitea
To migrate a repo with its metadata from a Gitea instance, you will first need to [create an access token](/advanced/access-token/) on the Gitea instance with your repository. Don't forget to delete the access token when you are finished.

> You can also use this method to migrate your repos away from Codeberg to another Gitea instance.

<picture>
  <source srcset="/assets/images/advanced/migrating-repos/gitea-migration.webp" type="image/webp">
  <img src="/assets/images/advanced/migrating-repos/gitea-migration.png">
</picture>

Here's an explanation of some fields on the [Gitea migration page](https://codeberg.org/repo/migrate?service_type=3):
- **Migrate / Clone From URL**: This is the URL to your repository. For example: `https://gitea.com/gitea/tea`.
- **Access Token**: You will paste the access token generated here. An access token is required to migrate metadata.
- **Migration Items**: Here you can select the metadata you want migrated.

## Starting Migration

Once you've filled out all the fields, click the `Migrate Repository` button.
Migration might take a while, depending on how large the repo is.

When the repo code appears, migration is complete!

> **Troubleshooting**: The migration process may time out after a while. If this happens, or if you have any other issues with migration, feel free to open an issue in the [Community Issue Tracker](https://codeberg.org/Codeberg/Community/issues).