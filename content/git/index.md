---
eleventyNavigation:
  key: Git
  title: Working with Git Repositories
  icon: code-branch
  order: 30
---

On these pages, you will learn how to use the Git version control system
with Codeberg.

There are 2 ways to interact with a Git repository hosted on Codeberg:
1. [via the CLI](/git/clone-commit-via-cli), either through SSH or HTTP.
2. [Using the website](/git/clone-commit-via-web/)

Option 1 requires a Git client of your choice [installed on your local system](/getting-started/install-git/).

We recommend the use of the [SSH protocol](https://en.wikipedia.org/wiki/Secure_Shell_Protocol).   
It offers improved security through key-based access (stronger protection than a regular password) and better usability (no need to provide credentials on every Git action).
