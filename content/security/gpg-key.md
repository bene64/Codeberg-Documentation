---
eleventyNavigation:
  key: GPGKey
  title: Adding a GPG key to your account
  parent: Security
---

## What is a GPG key?
GPG stands for GNU Privacy Guard, which is an open-source cryptographic software program and uses an implementation of the [Open PGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy#OpenPGP) standard. A GPG key is used to sign each commit you make so your collaborators can be sure it was you who made it.

## Adding an Existing GPG key
If you have your public key in an easy to find location, great! You can skip to [adding it to your account](#adding-your-gpg-key-to-codeberg). If not we will be using the [GnuPG software](https://gnupg.org/download/index.html#binary) to check, if you have downloaded this before you can skip to [Generating a GPG key](#generating-a-gpg-key).

1. Download and install [GnuPG](https://gnupg.org/download/index.html#binary).
> If you are using a Linux distro this might already be installed, check by typing `gpg  --version` in the terminal.

2. Type `gpg --list-secret-keys --keyid-format LONG` into your terminal, it will list all the keys that you have both a public and private key for.
3. Check the output for a key that you want, if there are none, or none that you want to use, continue to [Generating a GPG key](#generating-a-gpg-key). If there is one that you want to use, skip to [adding it to your account](#adding-your-gpg-key-to-codeberg).
> **IMPORTANT**   
> Be sure that your selected key uses the same email as your Codeberg account.

## Generating a GPG key
If you haven’t already, be sure to [install](https://gnupg.org/download/index.html#binary) GnuPG, as you will be using it to generate your keys.

1. Type `gpg --full-generate-key` into your terminal
2. Type `1` and press enter to select `RSA and RSA`
3. Enter your desired key size, we recommend 4096 bits, press enter to confirm
4. Choose the amount of time you want it to be valid for, we recommend 1-2 years for increased security, however you can type 0 for it to never expire. Press enter to confirm.
5. Verify your selections are correct, type `y` and press enter to confirm
6. Enter your information, be sure to use the same email as your Codeberg account
7. Type in a passphrase, make sure you write it down somewhere safe. You'll need it later to add your key to Git or revoke it if it is compromised

## Adding your GPG key to Codeberg

1. Type `gpg --list-secret-keys --keyid-format LONG` into the terminal
2. Select the key you would like to use (the one you just generated). In this example, the GPG key ID is `3AA5C34371567BD2`:
```shell
$ gpg --list-secret-keys --keyid-format LONG
/home/knut/.gnupg/pubring.kbx
--------------------------
sec   rsa4096/3AA5C34371567BD2 2021-06-06 [SC] [expires: 2022-06-06]
      6CD8F2B4F3E2E8F08274B563480F8962730149C7
uid                 [ultimate] knut <knut@codeberg.org>
ssb   rsa4096/42B317FD4BA89E7A 2021-06-06 [E] [expires: 2022-06-06]
```
3. Type `gpg --armor --export <GPG KEY ID>` into the terminal
4. Copy the output including the `-----BEGIN PGP PUBLIC KEY BLOCK-----` and `-----END PGP PUBLIC KEY BLOCK-----`
5. Go to the [SSH/GPG Keys tab](https://codeberg.org/user/settings/keys) in your Codeberg settings.
6. Click `Add Key` in the `Manage GPG Keys` section, paste in your public key and click the `Add Key` button.

## Telling Git about your GPG key
In order for you to use your key you will also need to tell Git about it.

1. Open your terminal
2. Type `git config --global user.signingkey <GPG KEY ID>`

---
> **Attribution**  
> This guide is derived from [GitHub Docs](https://docs.github.com), used under CC-BY 4.0.
