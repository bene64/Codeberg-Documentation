---
eleventyNavigation:
  key: SSHFingerprint
  title: Verifying you're connected to Codeberg
  parent: Security
---

When you connect to Codeberg via SSH, for example
[to clone or commit](/git/clone-commit-via-cli/), you need to make sure that
you're actually connected to Codeberg's servers and not someone else's server
attempting to execute a so-called [man-in-the-middle attack](https://en.wikipedia.org/wiki/Man-in-the-middle_attack).

To protect you against this sort of attacks, SSH will ask you the first time
you connect to a new server, whether you want to trust that server:

```bash
$ git clone git@codeberg.org:Codeberg/Documentation
Cloning into 'Documentation' ...
The authenticity of host 'codeberg.org (159.69.0.178)' can't be established.
ECDSA key fingerprint is SHA256:T9FYDEHELhVkulEKKwge5aVhVTbqCW0MIRwAfpARs/E.
Are you sure you want to continue connecting (yes/no/[fingerprint])? 
```

When connecting to Codeberg, it is important that you check the displayed fingerprint
against one of the following fingerprints published by Codeberg:

```
SHA256:6QQmYi4ppFS4/+zSZ5S4IU+4sa6rwvQ4PbhCtPEBekQ codeberg.org (RSA)
SHA256:T9FYDEHELhVkulEKKwge5aVhVTbqCW0MIRwAfpARs/E codeberg.org (ECDSA)
SHA256:mIlxA9k46MmM6qdJOdMnAQpzGxF4WIVVL+fj+wZbw0g codeberg.org (ED25519)
```

These are the SHA256 versions of **[the fingerprints published in the Imprint](https://codeberg.org/codeberg/org/src/branch/master/Imprint.md#user-content-ssh-fingerprints)**,
which are to be considered the authoritative fingerprints for Codeberg.

**If they match, you're good to go** and can safely use Codeberg via SSH.

**If they don't, don't continue to connect**, because your credentials may be at risk
and please give us a heads-up at [contact@codeberg.org](mailto:contact@codeberg.org).