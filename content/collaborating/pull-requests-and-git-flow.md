---
eleventyNavigation:
  key: PullRequestsGitFlow
  title: Pull requests and Git flow
  parent: Collaborating
  order: 20
---

## Benefits of a pull-request based workflow

> ***TLDR:*** *Keep an eye on your repository and org permissions. Don't take sweets from strangers. Use pull requests. Easy to review, easy to manage, and only the project maintainers/owners need full access to the repo to merge them.*

Although it is perfectly possible to use a Git project on Codeberg just as single shared central repository for individuals and teams, a collaborative workflow based on pull requests provides many benefits:

- The "hot" project repository requires only very few maintainers with full rights to sign off pull requests. Contributors can easily work on forked repositories.
- Each pull request collects the full edit history for a fix or feature branch. Contributors can squash this, or keep it, just as they prefer.

### Cheat sheet

Let's say, you would like to contribute to project [Codeberg/build-deploy-gitea](https://codeberg.org/Codeberg/build-deploy-gitea).

First, fork the project you would like to work on, by clicking the `fork` button in the top-right corner of the project page:

![Fork a project](/assets/images/collaborating/pull-requests-and-git-flow/fork-button.png)

Then clone it onto your local machine. We assume that [you have set up your SSH keys](/security/ssh-key). This has to be done only once:

```shell
git clone git@codeberg.org:<YOURCODEBERGUSERNAME>/build-deploy-gitea.git
```

Now, let's create a feature branch, do some changes, commit, push, edit, commit, push, ..., edit, commit, push:

```shell
git checkout -b my_cool_feature_branch
## do some changes
git commit -m "first feature"
git push                        ## here you get asked to set your upstream URL, just confirm
## do more work, edit...
git add new_file.png
git commit -m "second feature introducing a new file"
git push
## ...
git commit -m "more work, tidy-up"
git push
```

Now you can create the pull request by selecting your feature branch, and clicking on the pull request button:

![Create a pull request](/assets/images/collaborating/pull-requests-and-git-flow/pull-request-button.png)

### Keep it up-to-date: rebase pull requests to upstream

Sometimes the upstream project repository is evolving while we are working on a feature branch, and we need to rebase and resolve merge conflicts for upstream changes into our feature branch. This is not hard:

In order to track the `upstream` repository, we add a second remote that is pointing to the original project. This has to be done only once:

```shell
git remote add upstream git@codeberg.org:Codeberg/build-deploy-gitea.git
```

Now, let's pull from `upstream`, and rebase our local branch against the latest `HEAD` of the upstream project repository:

```shell
git pull --rebase upstream master
git pull
```

That's it. You can now push your changes, and create the pull request as usual by clicking on the "pull request" button.

## A friendly note on owner rights, and forced push permissions

Please keep in mind that project owners can do *everything*, including editing and rewriting the history using `force-push`. In some cases this is a useful feature (for example to undo accidental commits that, say, leaked credentials), but in most cases a transparent history based on a pull-request based workflow is surely preferable.
