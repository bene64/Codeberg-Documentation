---
eleventyNavigation:
  key: FirstSteps
  title: Your First Steps on Codeberg
  parent: GettingStarted
  order: 20
---

## Registering at Codeberg.org
To create your account at Codeberg.org, visit [https://codeberg.org](https://codeberg.org)
and click on "Register" to bring up the registration form.

There, simply fill in your username and email address and then choose a good password.

After confirming your email address by clicking the link that we sent you to your
email account, you're done and good to go!


## Community
Codeberg is a non-profit volunteer effort. It relies on its friendly and dedicated community. We're happy that you're now a part of our community, too, and we hope you're feeling welcome!

We kindly ask you to be polite and civil when on Codeberg and to not excessively use the resources provided. Please see our [Terms of Use](https://codeberg.org/codeberg/org/src/branch/master/TermsOfUse.md) for details.

Please remember that everyone here is contributing on their free time, as volunteers.

If you like a project on Codeberg, consider **giving it a star** and/or **following its author**. That helps with building and growing a network of free software projects and people and can serve as guidance for users exploring Codeberg, looking for interesting projects to try out or contribute to.


## Orienting yourself on Codeberg
After successfully registering on Codeberg, you should see this screen:

<picture>
  <source srcset="/assets/images/getting-started/first-steps/dashboard.webp" type="image/webp">
  <img src="/assets/images/getting-started/first-steps/dashboard.png" alt="Dashboard">
</picture>

An explanation of the highlighted elements above:

- **Dashboard (1)** is the screen you're looking at right now. It gives you an overview of the newest activity
  by you, the people you follow and in your or your organization's projects.
- **Issues (2)** and **Pull Requests (3)** These are overviews of issues and pull requests that you are either involved in or that are part of one of the organizations you belong to.
- **Codeberg Issues (4)** This is currently the primary communication channel for the Codeberg Community. Here, you can ask questions, report bugs and suggest changes to Codeberg. You're also welcome to browse these issues and contribute your solutions, if you want to support Codeberg even more.
- **The Create Menu (5)** You can create repositories, organizations and migrations using this menu. It's your entry point to kicking off entirely new things on Codeberg.
- **Your Avatar (6)** will lead you to a menu, where you can look at your profile, edit your settings, view the documentation of Gitea (the software Codeberg is based on) or log out.
- **The Dashboard Context Switcher (7)** If you're part of one or more organizations, you can use this context switcher to let the dashboard display information relevant to the organization, rather than your account.


## A word about Security
While it is important to choose a strong password, it is also
important to use Two-factor Authentication in case your password or device
ever gets compromised, so that should be one of the first things to
configure after you have created your account.

We recommend that you use Two-factor Authentication on your account, to increase
your security.

> Please visit the [guide in the security section](/security/2fa) to learn how to set up 2FA.


## Moving on from here
Now that you have an account on Codeberg.org, you can choose from a number of possible ways to explore:

- [Create your first Repository](/getting-started/first-repository)
- [Report or Comment on an Issue](/getting-started/issue-tracking-basics)
- [Learn how to use Git](/git) and then [Contribute a Pull Request](/collaborating/pull-requests-and-git-flow)
- [Contribute to Codeberg](/improving-codeberg)