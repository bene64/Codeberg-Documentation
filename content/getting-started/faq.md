---
eleventyNavigation:
  key: FAQ
  title: Frequently Asked Questions
  parent: GettingStarted
  order: 80
---

## Is Codeberg well funded?
Codeberg is primarily funded by donations. As of July 2020, with all expenses frozen, we have a runway of ~12 years, so you don't have to worry that our service suddenly disappears.  
Still, we can always make good use of donations, they will be used to build new features and extend our services.

## Where is Codeberg hosted?
We are hosted in Germany by netcup GmbH as well as on own hardware in a rented facility in Berlin, Germany.

## Is it allowed to host non-free software?
Our mission is to support the development and creation of Free Software, thus we only allow those repos licensed under an OSI/FSF-approved license.
Please do not use custom licenses, [they are usually a bad idea](https://en.wikipedia.org/wiki/License_proliferation) and might result in legal uncertainty (can I really really use this project or might a terribly-written-license put me as a user in danger?).  
However, we sometimes tolerate repositories that aren't perfectly licensed correctly and focus on spreading awareness on the topic of improper FLOSS licensing and it's issues.

## Can I host private repositories?
Codeberg is primarily intended for public and free content. However, as per our [Terms of Service](https://codeberg.org/codeberg/org/src/TermsOfUse.md#repositories-wikis-and-issue-trackers), "Private repositories may be used for example to store keychains in build systems and continuous integration tools, or as staging areas for pre-alpha tests within limited groups.". As usual, common sense applies, so you can also use it for note-taking or internal team communication, if the resource demand stays reasonable.

## What is the size limit for my repositories?
There is no fixed limit, but use cases that harm other users and projects due to excessive resource impact will get restricted. Please refer to our [terms of service](https://codeberg.org/codeberg/org/src/TermsOfUse.md#repositories-wikis-and-issue-trackers).

## What is the size limit for my avatar?
You can upload avatar pictures of up to 1 megabyte and 1024x1024 resolution.

## Is Codeberg open-source?
Codeberg is built on [Gitea](https://github.com/go-gitea/gitea), which is open-source. We make all of our changes and other code available under the [Codeberg organization](https://codeberg.org/Codeberg).

## What version of Gitea is Codeberg currently running?
You can check the version of Gitea that Codeberg uses through the [API here](https://codeberg.org/api/v1/version).

You will get a response like this: `{"version":"1.12.3+20-gb49f2abc5"}`. Here, 1.12.3 is the Gitea version number, and 20 is the number of patches applied on top of the release (which for example includes upstream commits and patches by Codeberg), with b49f2abc5 being the commit ID of the last patch applied on [the Codeberg branch](https://codeberg.org/Codeberg/gitea/commits/branch/codeberg) of Codeberg's Gitea repository. Note that the commit ID is without the leading "g".
