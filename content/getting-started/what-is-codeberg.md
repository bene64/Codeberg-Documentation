---
eleventyNavigation:
  key: WhatIsCodeberg
  title: What is Codeberg?
  parent: GettingStarted
  order: 10
---

Codeberg is a community-driven, non-profit software development platform operated
by Codeberg e.V. and centered around Codeberg.org, a Gitea-based software forge.

On Codeberg you can develop your own [Free Software](https://simple.wikipedia.org/wiki/Free_software) projects, contribute to other
projects, [browse](https://codeberg.org/explore) through inspiring and useful
free software, share your knowledge or build your projects a home on the web
using [Codeberg Pages](/codeberg-pages), just to name a few.

Codeberg is not a corporation but a community of free software enthusiasts providing
a humane, non-commercial and privacy-friendly alternative to commercial services
such as GitHub.

## Our Mission

The development of Free and Open Source Software is experiencing an unbroken boom, due to the general availability of the internet and the resulting social network effects, multiplying communication, exchange of ideas, and productivity each and every month.
The number of developers and projects participating in the Open-Source movement is growing exponentially.
Only new software tools and collaboration platforms made these dynamics possible and manageable.

While all successful software tools that enabled this development were contributed by the Free and Open Source Software community, commercial for-profit platforms dominate the hosting of the results of our collaborative work.
This has led to the paradox that literally millions of volunteers create, collect, and maintain invaluable knowledge, documentation, and software, to feed closed platforms driven by commercial interests, whose program is neither visible nor controllable from outside.
Considering the fate of formerly successful startups like SourceForge, we need to break the circle and avoid history repeating.

The mission of the Codeberg e.V. is to build and maintain a free collaboration platform for creating, archiving, and preserving code and to document its development process.

Dependencies on commercial, external, or proprietary services for the operation of the platform are thus decidedly avoided, in order to guarantee independence and reliability.

[Read more at Codeberg's intial announcement](https://blog.codeberg.org/codebergorg-launched.html).

## Codeberg vs. Gitea

[Gitea](https://gitea.io) is a free software for Git-based software development that powers Codeberg. Compared to Codeberg, Gitea is not a hosted service, but the free software tool to build those. Everyone can install their own Gitea instance to host their own projects. There are also some Gitea instances next to Codeberg you can use, but please make sure you find a site that is actively maintained and updated, and that you trust the provider.
Beware: The official Gitea instance at [gitea.com](https://gitea.com) is only meant for development of Gitea and related products and not public use!

People are often asking why they should use Codeberg over other Gitea instances. The most important points Codeberg adds are
- a vivid community to collaborate with and ask for help
- active maintenance through the community and shared effort to provide an awesome experience
- you can take part in operation and decisions, and ideally donate
- we add additional services like [Codeberg Pages](/codeberg-pages/) and in the future hosted CI

## What is Codeberg e.V.?

Codeberg e.V. is a registered non-profit association based in Berlin, Germany. You don't have to be
a member of the association in order to join Codeberg.org or to contribute to the development
of the platform, but if you want you can [join Codeberg e.V.](https://join.codeberg.org) to
support the project financially, be informed about Codeberg and, optionally, to actively
contribute to the association.

Codeberg members can also take part in the decisions of the platform as explained in the bylaws, and they elect the praesidium and board of the platform, thus Codeberg can be considered as community-owned.

---

To start your journey with Codeberg, let's [create an account](/getting-started/first-steps).
